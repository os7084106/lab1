#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>

#define BUFFER_SIZE 10

int main(void) {
    int pipe1[2];  
    int pipe2[2]; 

    char buffer[BUFFER_SIZE];
    int i;

    if (pipe(pipe1) == -1) {
        perror("pipe");
        return 1;
    }
    if (pipe(pipe2) == -1) {
        perror("pipe");
        return 1;
    }
    switch (fork()) {
        case -1:    
            perror("fork");
            return 1;

        case 0: 
            close(pipe1[1]);
            read(pipe1[0], buffer, BUFFER_SIZE);
            printf("child: %s\n", buffer);

            for (i = 0; i < strlen(buffer); i++) {
                buffer[i] = toupper(buffer[i]);
            } 

            close(pipe2[0]);
            write(pipe2[1], buffer, strlen(buffer) + 1);
            break;

        default:
            close(pipe1[0]);
            write(pipe1[1], "Hello", 6);
            close(pipe2[1]);
            read(pipe2[0], buffer, BUFFER_SIZE);
            printf("parent: %s\n", buffer);
            break;
    }
    return 0;
}
